
function validateCaptcha() {
    UrusForm.validateCaptcha()
}

let UrusForm = new Vue({
    el: '#urus-eform-app',
    data: {
        isSlow: 0,
        ready: 0,
        sectionComplete: 0,
        domain: 'http://dev.urus.rhb.growthopsapp.com',
        // domain: 'http://172.30.227.42:81',
        // domain: 'http://172.30.227.42:82',
        // domain: 'http://172.30.81.57:82',
        testCheck: 1,
        sections: [
            { id: 1, title: "Personal Information", isActive: 1 },
            { id: 2, title: "Employment & Income", isActive: 0 },
            { id: 3, title: "Loan Financing & URUS Package", isActive: 0 },
            { id: 4, title: "Disclaimer & Consent", isActive: 0 },
            { id: 5, title: "Completed", isActive: 0 },
        ],
        activeSection: 0,
        sectionCount: 0,
        form: [],
        files: [],
        isHuman: 0,
        captchaBypass: 1,
        showDate: 0,
        date: '',
    },
    methods: {
        changeSection(nextPage) {
            this.form[this.activeSection].isActive = 0;
            this.sections[this.activeSection].isActive = 0;
            if (nextPage && this.sectionCount - this.activeSection != 1) this.activeSection++;
            else if (!nextPage && this.activeSection != 0) this.activeSection--;
            this.form[this.activeSection].isActive = 1;
            this.sections[this.activeSection].isActive = 1;
        },
        nextSection() {
            if (this.activeSection >= 4) window.location.reload()
            else if (this.activeSection == 3 && this.testCheck) {

                if (this.captchaBypass && this.formValidation(1))
                    this.validateCaptcha()
                else if (this.formValidation(1))
                    grecaptcha.execute();
                // if (this.formValidation(1) && this.isHuman) {
                //     this.changeSection(1)
                //     this.getData(1)
                // }
            }
            else if (this.testCheck == 0)
                this.changeSection(1)
            else if (this.formValidation(1)) {
                this.changeSection(1)
            }
        },
        prevSection() {
            this.changeSection(0)
            if (this.activeSection == 0) {
                setTimeout(() => {
                    this.setIDRegex(this.form[0].fields[1].value)
                }, 200);
            }
        },
        showErrorPage() {
            this.activeSection = 5
        },
        addLoanRow(type) {
            if (type == 'bankLoan') {
                let temp = { bankId: "", facilityId: "", accountNumber: '' }
                this.form[2].bankCount++
                this.form[2].bankLoans.push(temp)
            } else {
                let temp = { lender: "", facilityId: "", amount: '' }
                this.form[2].otherCount++
                this.form[2].otherLoans.push(temp)
            }
        },
        removeLoanRow(type) {
            if (type == 'bankLoan') {
                this.form[2].bankCount--
                this.form[2].bankLoans.pop()
            } else {
                this.form[2].otherCount--
                this.form[2].otherLoans.pop()
            }
        },
        validateCaptcha() {
            this.isHuman = true
            this.changeSection(1)
            this.getData(1)
            // document.querySelector('#captchaInput').setCustomValidity('Incorrect captcha')
        },
        checkInput(e) {
            e.target.setCustomValidity('')
        },
        showErrorMessage(msg, e) {
            if (msg)
                e.target.setCustomValidity(msg)
        },
        formValidation(opt) {
            let isComplete = ''
            if (opt)
                isComplete = document.querySelector("form").reportValidity() // show error message
            else
                isComplete = document.querySelector("form").checkValidity() // return true/false only

            if (isComplete) {
                this.sectionComplete = 1
                return true
            } else {
                // document.querySelector('input:invalid').focus()
                return false
            }
        },
        setIDRegex(val) {
            if (val == "5791ad8b-c997-ec11-b3fd-000d3ac95aaf") {
                this.form[0].fields[2].regex = "^([0-9]{12,12})*$"
                this.form[0].fields[2].errMsg = "Please enter 12 digits"
                // document.querySelector('#rhb-urus-id').type = 'number'
                document.querySelector('#rhb-urus-id').type = 'text'
            }
            else if (val != 0) {
                this.form[0].fields[2].regex = "^([a-zA-Z0-9]{1,})*$"
                this.form[0].fields[2].errMsg = "Please fill out this field."
                document.querySelector('#rhb-urus-id').type = 'text'
            }
        },
        setState(state) {
            this.form[0].fields[8].defaultOption = "--Please select--"
            this.form[0].activeState = state
            axios({
                method: 'get',
                url: `${this.domain}/api/v1/urus/states/${state}/cities`,
                responseType: 'json'
            })
                .then(function (response) {
                    return response.data.data
                }).then((data) => {
                    console.log('data: ', data);
                    this.form[0].fields[8].options = data
                })
                .catch(error => console.log(error));
        },
        updateFilename(e, i1, i2) {
            let tempFile = e.target.files || e.dataTransfer.files;
            if (!tempFile.length)
                return;
            this.files[i2] = tempFile[0]
            // Get the selected file
            const [file] = e.target.files;
            // Get the file name and size
            const { name: fileName, size } = file;
            // Convert size in bytes to kilo bytes
            const fileSize = (size / 1000).toFixed(2);
            if (fileSize > 5000) {
                alert("The file is too large. The file limit is 5Mb.")
                e.target.value = ""
            } else {
                // Set the text content
                const fileNameAndSize = `${fileName} - ${fileSize}Kb`;
                let filenameEl = e.path[1].lastChild
                this.form[i1].fields[i2].fileName = fileNameAndSize
                this.form[i1].fields[i2].hasFile = true
                console.log('FORM', this.form[i1]);
                filenameEl.textContent = fileNameAndSize
            }

        },
        showUpload(status) {
            console.log(status);
        },
        getData(isSubmitForm) {
            console.warn(this.files);
            //first object
            let obj = {};
            let formData = new FormData();
            formData.append('CustomerName', this.form[0].fields[0].value);
            formData.append('IdTypeId', this.form[0].fields[1].value);
            formData.append('CustomerIdNumber', this.form[0].fields[2].value);
            formData.append('DateOfBirth', this.date);
            formData.append('MobileNumber', this.form[0].fields[4].value);
            formData.append('AltMobileNumber', this.form[0].fields[5].value);
            formData.append('EmailAddress', this.form[0].fields[6].value);
            formData.append('StateId', this.form[0].fields[7].value);
            formData.append('CityId', this.form[0].fields[8].value);
            formData.append('ResidentialStatusId', this.form[0].fields[9].value);
            formData.append('MaritalStatusId', this.form[0].fields[10].value);
            formData.append('NumberOfDependants', this.form[0].fields[11].value);
            // employment
            if (this.form[1].fields[0].status == 1) formData.append('EmploymentStatusProofFile', this.files[0]);
            formData.append('EmploymentStatusId', this.form[1].fields[1].value);
            formData.append('PreviousMonthlyGrossIncome', this.form[1].fields[2].value);
            formData.append('PreviousMontlyGrossIncomeProofFile', this.files[2]);
            formData.append('LatestMonthlyGrossIncome', this.form[1].fields[3].value);
            formData.append('LatestMonthlyGrossIncomeProofFile', this.files[3]);
            formData.append('LatestMonthlyNetIncomeProofFile', this.files[4]);
            formData.append('latestMonthlyNetIncome ', this.form[1].fields[4].value);
            formData.append('SpouseEmploymentStatusId', this.form[1].fields[5].value);
            formData.append('SpouseLatestMonthlyGrossIncome', this.form[1].fields[6].value);

            console.group("Loans");
            console.log('this.form[2].bankLoans: ', this.form[2].bankLoans);
            console.log('this.form[2].otherLoans: ', this.form[2].otherLoans);

            this.form[2].bankLoans.forEach(bank => {
                formData.append('FinancingDetails', JSON.stringify(bank));
            });

            this.form[2].otherLoans.forEach((loan, i) => {
                if (this.form[2].otherLoans[i].lender == "" || this.form[2].otherLoans[i].facilityId == "" || this.form[2].otherLoans[i].amount == "") {
                    this.form[2].otherLoans.splice(0, i - 1)
                } else {
                    this.form[2].otherLoans[i].amount = parseInt(loan.amount)
                    formData.append('OtherLoanCommitments', JSON.stringify(this.form[2].otherLoans[i]));
                    // console.log('loan.amount: ', loan.amount);
                }
            });

            console.groupEnd();

            formData.append('UrusPackageOptionId', this.form[2].package.value);
            formData.append('UrusPackageOptionName', this.form[2].package.value);
            console.log(this.form);
            // formData.append('IsDeclaredAndConfirmed', this.isHuman);
            formData.append('IsDeclaredAndConfirmed', true); //! boolean

            // List key/value pairs
            for (let [name, value] of formData) {
                console.log(`${name} = ${JSON.stringify(value)}`); // key1 = value1, then key2 = value2
            }

            let test = new FormData();
            test.append('name', 'vlad put');

            if (isSubmitForm) {
                formData.append('IsDeclaredAndConfirmed', this.isHuman);
                this.submitForm(formData)
            }
        },
        submitForm(data) {
            axios.post(`${this.domain}/api/v1/urus/form-submissions`, data, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'accept': 'text/plain'
                }
            }).then(function (response) {
                console.log(response);
                // alert(`Status ${response.status}: ${response.statusText}`)
            }).catch(function (error) {
                console.log(error.response);
                if (error.response.status != 200) {
                    UrusForm.showErrorPage()
                    console.log(`status ${JSON.stringify(error.response.status)}: ${JSON.stringify(error.response.data.errors)}; ${JSON.stringify(error.response.data)}`)
                }
            });
        },
        insertData() {
            this.form[0].fields[0].value = "Soo Yong Jie"
            this.form[0].fields[1].value = "5791ad8b-c997-ec11-b3fd-000d3ac95aaf"
            this.form[0].fields[2].value = "000625100395"
            this.form[0].fields[3].value = "1997-05-21"
            this.form[0].fields[4].value = "011293812233"
            this.form[0].fields[5].value = "01048683227"
            this.form[0].fields[6].value = "soo.yongjie@growthops.asia"
            this.form[0].fields[7].value = "7bf7dd64-cb97-ec11-b3fd-000d3ac95aaf"
            setTimeout(() => {
                this.form[0].fields[8].value = "3b6bb07a-d097-ec11-b3fd-000d3ac95aaf"
            }, 1000);
            this.form[0].fields[9].value = "deab089c-d297-ec11-b3fd-000d3ac95aaf"
            this.form[0].fields[10].value = "c31c9735-ca97-ec11-b3fd-000d3ac95aaf"
            this.form[1].fields[1].value = "aa99ad63-d397-ec11-b3fd-000d3ac95aaf"
            this.form[1].fields[2].value = "69"
            this.form[1].fields[3].value = "69"
            this.form[1].fields[4].value = "69"
            this.form[1].fields[5].value = "03a3d76c-d397-ec11-b3fd-000d3ac95aaf"
            this.form[1].fields[6].value = "0"
            this.form[2].bankLoans[0] = JSON.parse("{\"bankId\":\"38dd051f-e497-ec11-b3fd-000d3ac95aaf\",\"facilityId\":\"ac24e11d-c597-ec11-b3fd-000d3ac95aaf\",\"accountNumber\":\"1\"}")
            this.form[2].otherLoans[0] = JSON.parse("{\"lender\":\"Mom\",\"facilityId\":\"f2808440-c597-ec11-b3fd-000d3ac95aaf\",\"amount\":69}")
            this.form[2].package.value = "0cd9036a-3c9a-ec11-b3fd-000d3ac95aaf"
        }
    },
    watch: {
        date() {
            this.date = moment(this.date).format('yyyy-MM-DD')
        } 
    },
    mounted() {
        setTimeout(() => {
            this.isSlow = true
        }, 6000);
        fetch('./data.json')
            .then(response => response.json())
            .then(data => {
                UrusForm._data.form = data
                this.sectionCount = this.form.length
            }).then(() => {
                axios({
                    method: 'get',
                    url: `${this.domain}/api/v1/urus/form-fields`,
                    responseType: 'json'
                })
                    .then(function (response) {
                        return response.data.data
                    }).then((data) => {

                        this.ready = 1
                        this.form[0].fields[1].options = data.idTypes
                        this.form[0].fields[7].options = data.states
                        this.form[0].fields[9].options = data.residentialStatuses
                        this.form[0].fields[10].options = data.maritalStatuses
                        this.form[1].fields[1].options = data.employmentStatuses
                        this.form[1].fields[5].options = data.spouseEmploymentStatuses
                        this.form[2].banks = data.banks
                        this.form[2].facilities = data.financingFacilities
                        this.form[2].package.options = data.urusPackageOptions
                    })
                    .catch(error => console.log(error));
            })
            .catch(error => console.log(error));
    }
})