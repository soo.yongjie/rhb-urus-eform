let rhbAdmin = new Vue({
    el: '#rhb',
    data: {
        modal: 0,
        startDate: "",
        endDate: "",
        startPage: 1,
        rowsPerPage: 10,
        totalSubmissions: 29,
        totalPages: 1,
        endPage: 3,
        currentPage: 1,
        paginationBuffer1: 2,
        paginationBuffer2: 3,
        paginationLimitCount: 2,
        applicationID: '',
        isFetchingPage: true,
        showToast: false,
        toastMessage: "Filtered successfully",
        isUpdatingStatus: false,
        showDate: 0,
        showStartDate: 0,
        showEndDate: 0,
        date: '',
        startDate: '',
        endDate: '',
        domain: 'http://dev.admin.urus.rhb.growthopsapp.com',
        // domain: 'http://172.30.227.42:81',
        // domain: 'http://172.30.227.42:82',
        // domain: 'http://172.30.81.57:82',
        cors: '',
        columns: [
            { name: "Doc ID" },
            { name: "Name" },
            { name: "Employment" },
            { name: "Date" },
            { name: "Status" },
        ],
        detailedColumns: [
            [
                { name: "Name" },
                { name: "ID type" },
                { name: "ID" },
                { name: "Date" },
                { name: "Phone number" },
                { name: "Phone number 2" },
                { name: "Email" },
                { name: "State" },
                { name: "City" },
                { name: "Residential status" },
                { name: "Marital status" },
                { name: "Dependants" },
                { name: "isDeclaredAndConfirmed" },
            ],
            [
                { name: "Employed" },
                { name: "Previous monthly Gross income" },
                { name: "Latest monthly Gross income" },
                { name: "Latest monthly net income" },
                { name: "Spouse employment status" },
                { name: "Spouse income" },
                { name: "Files" },
            ],
        ],
        detailsModal: [],
        fields: []
    },
    methods: {
        fetchData() {
            this.isFetchingPage = true
            axios({
                method: 'get',
                url: `${this.cors}${this.domain}/api/v1/urus/submissions/${this.startPage}/${this.rowsPerPage}?startDate=${this.startDate}&endDate=${this.endDate}`,
                headers: { 'Access-Control-Allow-Origin': '*' },
                responseType: 'json'
            })
                .then(function (response) {
                    return response.data
                }).then((data) => {
                    this.isFetchingPage = false
                    console.log(data);
                    this.fields = data.data.submissions
                    this.totalSubmissions = data.data.totalSubmissions
                    this.totalPages = Math.trunc(this.totalSubmissions / this.rowsPerPage) + 1
                    if (this.totalSubmissions == 0) {
                        this.toastMessage = "No results found"
                        this.showToast = true
                        alert('No results')
                    }
                })
                .catch(error => console.log(error));
        },
        findApplicationByID() {
            return this.fields.find(({ id }) => id === this.applicationID);
        },
        showDetails(id) {
            this.applicationID = id
            let result = this.findApplicationByID()
            this.detailsModal = result
            // this.detailsModal = this.fields[id].details
            console.log('this.detailsModal: ', this.detailsModal);
            this.modal = 1
        },
        checkDates() {
            const date1 = new Date(this.startDate);
            const date2 = new Date(this.endDate);

            if (date1 > date2) {
                alert("Start date must be before End date")
                return false
            } else if (date1 == "Invalid Date") {
                alert("Enter Start date")
                return false
            } else {
                console.log("Success");
                return true
            }
        },
        filter(e) {
            if (this.checkDates()) {
                this.isFetchingPage = true
                setTimeout(() => {
                    this.setPage()
                    // this.showToast = true
                }, 500);
            }
        },
        extract(e) {
            if (this.checkDates()) {
                console.warn("Extract!");
                window.open(`${this.cors}${this.domain}/api/v1/urus/submissions/download/csv?bankCode=0&startDate=${this.startDate}&endDate=${this.endDate}&formType=URS`)
                axios({
                    method: 'get',
                    url: `${this.cors}${this.domain}/api/v1/urus/submissions/download/csv?bankCode=0&startDate=${this.startDate}&endDate=${this.endDate}&formType=URS`,
                })
                    .then(function (response) {
                        console.log('response: ', response.status);
                    })
                    .catch(error => console.log(error));
            }
        },
        setPage() {
            setTimeout(() => {
                let url = window.location.href
                let urlPage = url.slice(url.lastIndexOf('#') + 1, url.lastIndexOf('#') + url.length)

                if (Number.isInteger(parseInt(urlPage))) {
                    this.paginationBuffer2 = parseInt(urlPage) + this.paginationLimitCount
                    this.startPage = urlPage
                    this.currentPage = this.startPage
                    this.paginationBuffer1 = this.currentPage - this.paginationLimitCount
                    this.fetchData()
                }
                else {
                    this.startPage = 1
                    this.currentPage = 1
                    this.fetchData()
                }
            }, 1);
        },
        changePage(n, option) {
            if (option == '+') {
                this.currentPage++;
                document.querySelectorAll('.pagination a')[n].click()
            } else {
                this.currentPage--;
                --n
                document.querySelectorAll('.pagination a')[--n].click()
            }
        },
        updateStatus(isApproved) {
            let api = ''
            let selectedApplication = ({ id }) => id === this.applicationID;
            let i = this.fields.findIndex(selectedApplication)

            if (isApproved) {
                api = `${this.domain}/api/v1/urus/submissions/${this.applicationID}/approve?sitecoreUsername=admin`
                this.fields[i].approvalStatus = 'approved'
                this.fields[i].isApproved = true
            } else {
                api = `${this.domain}/api/v1/urus/submissions/${this.applicationID}/reject?sitecoreUsername=admin`
                this.fields[i].approvalStatus = 'rejected'
                this.fields[i].isApproved = false
            }
            axios.post(`${this.cors}${api}`, {
            }).then(function (response) {
                console.log(response);
                // alert(`Status ${response.status}: ${response.statusText}`)
            }).catch(function (error) {
                this.isUpdatingStatus = false
                console.log(error.response);
                // alert(`status ${JSON.stringify(error.response.status)}: ${JSON.stringify(error.response.data.errors)}; ${JSON.stringify(error.response.data)}`)
            });
        }
    },
    watch: {
        showToast: function () {
            setTimeout(() => {
                this.showToast = false
            }, 2000);
        },
        startDate() {
            this.startDate = moment(this.startDate).format('yyyy-MM-DD')
        },
        endDate() {
            this.endDate = moment(this.endDate).format('yyyy-MM-DD')
        },
    },
    mounted() {
        fetch('./data.json')
            .then(response => response.json())
            .then(data => {
                this.fields = data
            })
            .catch(error => console.log(error));
        this.endDate = moment().format('YYYY-MM-DD')
        this.startDate = moment().subtract(1, 'month').format('YYYY-MM-DD')
        window.onpopstate = function (event) {
            rhbAdmin.setPage();
        };
        this.setPage()

    }
})